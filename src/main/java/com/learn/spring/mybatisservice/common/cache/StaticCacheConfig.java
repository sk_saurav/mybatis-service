/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.common.cache;

import com.learn.spring.mybatisservice.dao.UserDao;
import com.learn.spring.mybatisservice.dao.UserInfoDao;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.Users;
import com.learn.spring.mybatisservice.service.IStartUp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

/**
 * The type Static cache config.
 *
 * @author kumar
 * @project mybatis -service
 * @since 8 /8/2020
 */
@Component(value = "staticCacheConfig")
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class StaticCacheConfig implements IStartUp {
    /**
     * logger instance of the class
     */
    private static final Logger log = LoggerFactory.getLogger(StaticCacheConfig.class);

    /**
     * The Users map.
     */
    private Map<String, Users> usersMap = null;

    /**
     * The Indus type info map.
     */
    private Map<String, IndusTypeInfo> indusTypeInfoMap = null;

    /**
     * The User dao.
     */
    @Autowired private UserDao userDao;

    /**
     * The User info dao.
     */
    @Autowired private UserInfoDao userInfoDao;

    /**
     * Start up boolean.
     *
     * @return the boolean
     */
    @Override
    @PostConstruct
    public boolean startUp() {
        log.info("startup static cache");
        boolean result = initialise();
        if (!result) {
            log.error("startup initialisation failed");
        }
        log.info("startup initialised cache completely with result {}.", result);
        return result;
    }

    /**
     * Initialise boolean.
     *
     * @return the boolean
     */
    private boolean initialise() {
        log.info("initialise staticCacheConfig");
        boolean result = true;
        result = result && loadUserCache();
        result = result && loadIndustryCache();
        log.info("initialised with result {}.", result);
        return result;
    }

    /**
     * Load user cache boolean.
     *
     * @return the boolean
     */
    public boolean loadUserCache() {
        log.info("userConfigCache invoked successfully...");
        List<Users> userList = null;
        CacheConfigVo cacheConfigVo = null;
        try {
            userList = userDao.findAllUserData();
            Map<String, Users> tempUserMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(userList)) {
                tempUserMap = userList.stream()
                        .collect(toMap(Users::getUserID, userdata -> userdata));
                if (!CollectionUtils.isEmpty(tempUserMap)) {
                    usersMap = tempUserMap;
                }
            }
        } catch (Exception e) {
            log.error("loadUserCache getting error with DB", e.getMessage());
            return false;
        }
        log.info("Logging dimension map data {}", String.valueOf(usersMap));
        log.info("Returning from userConfigCache ");
        return true;
    }

    /**
     * Load industry cache boolean.
     *
     * @return the boolean
     */
    public boolean loadIndustryCache() {
        log.info("loadDimensionCache invoked successfully...");
        CacheConfigVo cacheConfigVo = null;
        List<IndusTypeInfo> indusTypeInfos = null;
        try {
            indusTypeInfos = userInfoDao.getAllIndustry();
            Map<String, IndusTypeInfo> tempIndustryMap = new HashMap<>();
            if (!CollectionUtils.isEmpty(indusTypeInfos)) {
                tempIndustryMap =
                        indusTypeInfos.stream()
                                .collect(
                                        toMap(
                                                IndusTypeInfo::getIndusTypeName,
                                                industryInfo -> industryInfo));
                if (!CollectionUtils.isEmpty(tempIndustryMap)) {
                    indusTypeInfoMap = tempIndustryMap;
                }
            }
        } catch (Exception e) {
            log.error("loadDimensionCache failed to load data into cache..." + e.getMessage());
            return false;
        }
        log.info("Logging dimension map data {}", String.valueOf(indusTypeInfoMap));
        log.info("Returning from loadDimensionCache ");
        return true;
    }

    /**
     * Gets users cache.
     *
     * @return the users cache
     */
    public Map<String, Users> getUsersCache() {
        return usersMap;
    }

    /**
     * Gets indus type infos.
     *
     * @return the indus type infos
     */
    public List<IndusTypeInfo> getIndusTypeInfos() {
        return new ArrayList<>(indusTypeInfoMap.values());
    }

    /**
     * Gets users info.
     *
     * @return the users info
     */
    public List<Users> getUsersInfo() {
        return new ArrayList<>(usersMap.values());
    }

    /**
     * Gets indus type cache.
     *
     * @return the indus type cache
     */
    public Map<String, IndusTypeInfo> getIndusTypeCache() {
        return indusTypeInfoMap;
    }
}
