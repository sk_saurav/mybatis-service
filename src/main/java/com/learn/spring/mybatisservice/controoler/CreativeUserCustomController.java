/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.controoler;

import com.learn.spring.mybatisservice.dao.UserInfoDao;
import com.learn.spring.mybatisservice.entity.CustomUserInfo;
import com.learn.spring.mybatisservice.entity.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class CreativeUserCustomController {
    @Autowired
    private UserInfoDao userInfoDao;

    @PostMapping("/user/custom/{userID}")
    public ResponseEntity<ResultVO<List<CustomUserInfo>>> updateCustom(@PathVariable String userID, @RequestBody CustomUserInfo customUserInfo) {
        List<CustomUserInfo> userInfo = Arrays.asList(userInfoDao.findUserInfoByID(userID));
        ResultVO<List<CustomUserInfo>> resultVO = new ResultVO<List<CustomUserInfo>>(userInfo);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("count","25");
        ResponseEntity<ResultVO<List<CustomUserInfo>>> data = new ResponseEntity<ResultVO<List<CustomUserInfo>>>(resultVO, httpHeaders , HttpStatus.OK);
        return data;
    }
}
