package com.learn.spring.mybatisservice.controoler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.spring.mybatisservice.common.cache.CacheConfigVo;
import com.learn.spring.mybatisservice.entity.IndusTypeInfo;
import com.learn.spring.mybatisservice.entity.ResultVO;
import com.learn.spring.mybatisservice.entity.Users;
import com.learn.spring.mybatisservice.response.ResultResponse;
import com.learn.spring.mybatisservice.service.UserAddService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * User controller to record User based request
 *
 * @author kumar
 * @project mybatis-service
 * @since 8/1/2020
 */
@RestController
public class UserController {

  /** logger added to record logs */
  private static final Logger logger = LoggerFactory.getLogger(UserController.class);

  /** userservice impl to record */
  @Autowired private UserAddService userAddService;

  @Autowired
  private CacheConfigVo configVo;

  @PostMapping("/v1/addUsers")
  public ResponseEntity<ResultResponse<Users>> addUsers(@RequestBody final List<Users> usersVo)
      throws Exception {
    logger.info("Entering into addUsers service with userVo: {}", usersVo);
    ResultResponse<Users> userInfo = userAddService.addUsers(usersVo);
    return ResponseEntity.ok(userInfo);
  }

  @GetMapping("/getAllIndustry")
  public Map<Integer, IndusTypeInfo> getAllIndustry() {
    return userAddService.getAllIndustry();
  }

  @GetMapping("getGeneratedKey")
  public int getGeneratedKey(final Users users) {
    return userAddService.getGeneratedKey(users);
  }

  @GetMapping("/userCache")
  public ResponseEntity<Map<String, Users>> getUserCache () {
    return ResponseEntity.ok(configVo.getUserCache());
  }

  @Autowired
  RestTemplate restTemplate;

  @GetMapping("/restTemplate")
  public ResponseEntity<String> getRestTemplate() {
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<String> entity = new HttpEntity<String>(headers);
    String data = restTemplate.exchange("http://localhost:8090/todo/api/v1.0/tasks", HttpMethod.GET, entity, String.class).getBody();
    ResultVO vo = new ResultVO("a", "b", data);
    return new ResponseEntity<String>(data, HttpStatus.OK);
  }

  @PostMapping("/restTemplate")
  public ResponseEntity<String> updateTemplate(@RequestBody Users users) {
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    HttpEntity<Users> entity = new HttpEntity<Users>(users, headers);
    String data = restTemplate.exchange("http://localhost:5000/update/user", HttpMethod.POST, entity, String.class).getBody();
    return new ResponseEntity<String>(data, HttpStatus.OK);
  }

  @GetMapping("/json/test")
  public Student getStudentData() throws ParseException, JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = simpleDateFormat.parse("21-08-1992");
    Student student = new Student("saurav", "12345", date);
    String str = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(student);
    System.out.printf("str: " + str);
    return student;
  }
}
