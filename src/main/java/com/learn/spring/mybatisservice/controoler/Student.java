/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.controoler;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Student {
    private String studentName;
    private String studentID;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date dob;

    public Student() {
    }

    @JsonCreator
    public Student(String studentName, @JsonProperty("Id") String studentID, @JsonProperty("dateOfBirth") Date dob) {
        this.studentName = studentName;
        this.studentID = studentID;
        this.dob = dob;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Student{");
        sb.append("studentName=").append(studentName);
        sb.append(", studentID=").append(studentID);
        sb.append(", dob=").append(dob);
        sb.append('}');
        return sb.toString();
    }
}
