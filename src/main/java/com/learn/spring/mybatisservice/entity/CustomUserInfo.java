/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CustomUserInfo {
    private String userName;
    private String modifiedBy;
}
