/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResultVO<T> {
    private String resultStatus;
    private String resultMsg;
    private String code;
    private T returnObject;

    public ResultVO(String resultStatus, String resultMsg, String code, T returnObject) {
        this.resultStatus = resultStatus;
        this.resultMsg = resultMsg;
        this.code = code;
        this.returnObject = returnObject;
    }

    public ResultVO(String resultStatus, String resultMsg, String code) {
        this.resultStatus = resultStatus;
        this.resultMsg = resultMsg;
        this.code = code;
    }

    public ResultVO() {
    }

    public ResultVO(T userInfo) {
        this.returnObject = userInfo;
    }
}
