package com.learn.spring.mybatisservice;

import org.apache.catalina.Manager;
import org.apache.catalina.session.StandardManager;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.ConfigurableTomcatWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

@SpringBootApplication
@MapperScan("com.learn.spring.mybatisservice.dao")
@EnableAsync
@EnableCaching
public class MybatisServiceApplication implements ServletContextInitializer, WebServerFactoryCustomizer<ConfigurableTomcatWebServerFactory> {

    public static void main(String[] args) {
        //System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(MybatisServiceApplication.class, args);
    }

    @Bean
    public Executor taskExecutors() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix("GithubLookup-");
        executor.initialize();
        return executor;
    }

    @Bean("CacheConfig")
    public List<String> cacheConfig() {
        List<String> cacheList = new ArrayList<>();
        cacheList.add("staticCacheConfig");
        cacheList.add("initializeCache");
        return cacheList;
    }

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.getSessionCookieConfig().setName("JSESSIONID");
    }

    @Override
    public void customize(ConfigurableTomcatWebServerFactory factory) {
        factory.addContextCustomizers((context) -> {
            Manager manager = new StandardManager();
            context.setManager(manager);
            context.getManager().getSessionIdGenerator().setSessionIdLength(24);
        });
    }
}
