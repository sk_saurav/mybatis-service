/*
 * Copy right from Saurav kumar
 */

package com.learn.spring.mybatisservice.service;

/**
 * The interface Start up.
 */
public interface IStartUp {

    /**
     * Start up boolean.
     *
     * @return the boolean
     */
    boolean startUp();
}
