/*
 * creative 3.0
 * creative 3.0
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package org.openapitools.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Users
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-10-24T00:36:01.809193500+05:30[Asia/Calcutta]")
public class Users {
  public static final String SERIALIZED_NAME_USER_ID = "userId";
  @SerializedName(SERIALIZED_NAME_USER_ID)
  private String userId;

  public static final String SERIALIZED_NAME_USER_NAME = "userName";
  @SerializedName(SERIALIZED_NAME_USER_NAME)
  private String userName;

  public static final String SERIALIZED_NAME_USER_EMAI = "userEmai";
  @SerializedName(SERIALIZED_NAME_USER_EMAI)
  private String userEmai;

  public static final String SERIALIZED_NAME_USER_PASSWORD = "userPassword";
  @SerializedName(SERIALIZED_NAME_USER_PASSWORD)
  private String userPassword;


  public Users userId(String userId) {
    
    this.userId = userId;
    return this;
  }

   /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(required = true, value = "")

  public String getUserId() {
    return userId;
  }


  public void setUserId(String userId) {
    this.userId = userId;
  }


  public Users userName(String userName) {
    
    this.userName = userName;
    return this;
  }

   /**
   * Get userName
   * @return userName
  **/
  @ApiModelProperty(required = true, value = "")

  public String getUserName() {
    return userName;
  }


  public void setUserName(String userName) {
    this.userName = userName;
  }


  public Users userEmai(String userEmai) {
    
    this.userEmai = userEmai;
    return this;
  }

   /**
   * Get userEmai
   * @return userEmai
  **/
  @javax.annotation.Nullable
  @ApiModelProperty(value = "")

  public String getUserEmai() {
    return userEmai;
  }


  public void setUserEmai(String userEmai) {
    this.userEmai = userEmai;
  }


  public Users userPassword(String userPassword) {
    
    this.userPassword = userPassword;
    return this;
  }

   /**
   * Get userPassword
   * @return userPassword
  **/
  @ApiModelProperty(required = true, value = "")

  public String getUserPassword() {
    return userPassword;
  }


  public void setUserPassword(String userPassword) {
    this.userPassword = userPassword;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Users users = (Users) o;
    return Objects.equals(this.userId, users.userId) &&
        Objects.equals(this.userName, users.userName) &&
        Objects.equals(this.userEmai, users.userEmai) &&
        Objects.equals(this.userPassword, users.userPassword);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, userName, userEmai, userPassword);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Users {\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    userName: ").append(toIndentedString(userName)).append("\n");
    sb.append("    userEmai: ").append(toIndentedString(userEmai)).append("\n");
    sb.append("    userPassword: ").append(toIndentedString(userPassword)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

