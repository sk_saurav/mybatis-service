

# IndustryInfoResp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**templateID** | **String** | Unique template Id | 
**templateName** | **String** | template name | 



