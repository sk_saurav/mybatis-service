

# IndustryInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | code | 
**message** | **String** | message | 
**status** | **String** | status | 
**retunObject** | [**List&lt;IndustryInfoResp&gt;**](IndustryInfoResp.md) |  |  [optional]



