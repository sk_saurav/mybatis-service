# IndustryApi

All URIs are relative to *http://localhost:8086*

Method | HTTP request | Description
------------- | ------------- | -------------
[**queryIndustries**](IndustryApi.md#queryIndustries) | **GET** /getAllIndustry | Api to return All indutry


<a name="queryIndustries"></a>
# **queryIndustries**
> IndustryInfo queryIndustries()

Api to return All indutry

### Example
```java
// Import classes:
import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.Configuration;
import org.openapitools.client.models.*;
import org.openapitools.client.api.IndustryApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost:8086");

    IndustryApi apiInstance = new IndustryApi(defaultClient);
    try {
      IndustryInfo result = apiInstance.queryIndustries();
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling IndustryApi#queryIndustries");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**IndustryInfo**](IndustryInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |
**400** | Bad Request |  -  |

